package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csvreader.CsvReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import API.ITaxiTripsManager;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.vo.Camino;
import model.vo.ComponenteConexo;
import model.vo.NodoArchivo;
import model.vo.Servicio;
import model.vo.ServicioGenerico;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.VerticeConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	public static final String DIRECCION_STREET_LINES = "./data/StreetLines.csv";
	private List<VerticeConServicios> listaVertices;
	ServicioGenerico[] servicios=null;
	@Override
	public boolean cargarSistema(String serviceFile) 
	{
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				System.out.println("Inside loadServices with " + serviceFile);
				
				BufferedReader reader=null;
				BufferedReader reader2=null;
				BufferedReader reader3=null;
				try
				{
					//Carga de datos y prueba
					reader=new BufferedReader(new FileReader(serviceFile));
					reader2=new BufferedReader(new FileReader(serviceFile));
					reader3=new BufferedReader(new FileReader(serviceFile));
					
					Gson gson1=new GsonBuilder().create();
					servicios=gson1.fromJson(reader, ServicioGenerico[].class);
					System.out.println("object mode: " + servicios[0].getCompany());
					
					//Carga de los vertices
					for(int i=0; i<servicios.length;i++)
					{
						ArrayList entran=new ArrayList();
						ArrayList salen=new ArrayList();
						ServicioGenerico actual=servicios[i];
						VerticeConServicios nuevo=new VerticeConServicios(Double.parseDouble(actual.getDropoff_centroid_latitude()),Double.parseDouble(actual.getDropoff_centroid_longitude()));
						for(int j=0; j <servicios.length;j++)
						{
							ServicioGenerico actual2=servicios[j];
							if(actual2.getDropoff_centroid_latitude().equals(actual.getDropoff_centroid_latitude()) && actual2.getDropoff_centroid_longitude().equals(actual.getDropoff_centroid_longitude()))
							{
								salen.add(actual2.getTrip_id());
							}
							else if(actual2.getDropoff_centroid_latitude().equals(actual.getDropoff_centroid_latitude()) && actual2.getDropoff_centroid_longitude().equals(actual.getDropoff_centroid_longitude()))
							{
								entran.add(actual2.getTrip_id());
							}
						}
						listaVertices.add(nuevo);
					}
					
				}
				catch(FileNotFoundException e)
				{
					System.out.println("No se encuentra el archivo");
				}
				finally
				{
					System.out.println("Listo!");
				}
				return false;
	}

	
	@Override
	public VerticeConServicios Req1VerticeMasCongestionado() {
		VerticeConServicios Rta = new VerticeConServicios(0, 0);
		int max = 0;
		for (int i=0; i< listaVertices.size(); i++)
		{
			if(((VerticeConServicios) listaVertices.get(i)).numeroServiciosTotal()>max)
			{
				max = ((VerticeConServicios) listaVertices.get(i)).numeroServiciosTotal();
				Rta= (VerticeConServicios) listaVertices.get(i);
			}
		}

		return Rta;
		}


	@Override
	public IList<ComponenteConexo> Req2ComponentesConexos() {
		// TODO Auto-generated method stub
		return new Lista<ComponenteConexo>();
	}


	@Override
	public void Req3GenerarMapaComponentes() 
	{
		double sumaTotal=0;
		for(int i=0; i<listaVertices.size();i++)
		{
			sumaTotal=sumaTotal+listaVertices.get(i).numeroServiciosQueLlegan()+listaVertices.get(i).numeroServiciosQueSalen();
		}
		
		for(int j=0; j<listaVertices.size();j++)
		{
			double porcentaje=(listaVertices.get(j).numeroServiciosQueLlegan()+listaVertices.get(j).numeroServiciosQueSalen())/sumaTotal;
			System.out.println(listaVertices.get(j).getLatRef()+porcentaje);
		}
	}


	@Override
	public Camino Req4CaminoCostoMinimo()
	{
		List<NodoArchivo> NodosSubidos = new ArrayList<NodoArchivo>();
		try {        
	        CsvReader reader = new CsvReader("docs/Chicago Streets.csv");
	        reader.readHeaders();
	         
	        while (reader.readRecord())
	        {
	            String nodoId = reader.get("TNODE_ID");
	            String preD = reader.get("PRE_DIR");
	            String StreetN = reader.get("STREET_NAM");
	            String StreetT = reader.get("STREET_TYP");
	            String oneW = reader.get("ONEWAY_DIR");
	            String theG = reader.get("the_geom");
	            String theG2 = reader.get("the_geom2");
	            NodosSubidos.add(new NodoArchivo(nodoId, StreetN, StreetT, preD,oneW,theG,theG2));    
	        }
	         
	        System.out.println(NodosSubidos.size());
	        reader.close();
	         
	        for(NodoArchivo us : NodosSubidos){
	         
	            System.out.println(us.getNodeId() + " : " + us.getStreetName() + " "
	            + us.getTheGeom() + " - " + us.getTheGeom2());
	        }
	         
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		
			int aleatorio=(int) (Math.random() * 30000) + 1;
			int aleatorio2=(int) (Math.random() * 30000) + 1;
			NodoArchivo nodoEmpieza=NodosSubidos.get(aleatorio);
			NodoArchivo nodoTermina=NodosSubidos.get(aleatorio2);
			
			
		
		return new Camino(0, 0, 0);
	}


	@Override
	public Camino[] Req5Caminos_Mayor_MenorDuracion() {
		// TODO Auto-generated method stub
		Camino mayorDuracion = new Camino(0, 0, 0);
		Camino menorDuracion = new Camino(0, 0, 0);
		Camino[] caminos = {mayorDuracion, menorDuracion}; 
		return caminos;
	}

	@Override
	public IList<Camino> Req6CaminosSinPeaje() {
		// TODO Auto-generated method stub
		return new Lista<Camino>();
	}





}
