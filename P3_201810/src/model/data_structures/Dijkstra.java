package model.data_structures;

/******************************************************************************
 * Modificación de:
 * 
 *  Compilation:  javac DijkstraSP.java
 *  Execution:    java DijkstraSP input.txt s
 *  Dependencies: EdgeWeightedDigraph.java IndexMinPQ.java Stack.java DirectedEdge.java
 *  Data files:   http://algs4.cs.princeton.edu/44sp/tinyEWD.txt
 *                http://algs4.cs.princeton.edu/44sp/mediumEWD.txt
 *                http://algs4.cs.princeton.edu/44sp/largeEWD.txt
 *
 *  Dijkstra's algorithm. Computes the shortest path tree.
 *  Assumes all weights are nonnegative.
 *
 * Sacado de http://algs4.cs.princeton.edu/44sp/DijkstraSP.java.html
 ******************************************************************************/

import java.util.Iterator;


public class Dijkstra{
	private double[] distTo;   
	private Arco[] edgeTo;    
	private ColaPrioridadMinIndex<Double> pq;
	private ListaEncadenada<Arco> l;

	public Dijkstra(WeightedDirectedGraph G, int s) 
	{
		distTo = new double[G.numVertices() + 1];
		edgeTo = new Arco[G.numVertices() + 1];

		validateVertex(s);
		for (int v = 0; v < G.numVertices(); v++)
		{
			distTo[v] = Double.POSITIVE_INFINITY;
			distTo[s] = 0.0;
		}

		l = new ListaEncadenada<>();
		pq = new ColaPrioridadMinIndex<Double>(G.numVertices());
		pq.insertar(s, distTo[s]);
		while (!pq.isEmpty()) {
			int v = pq.delMin();
			int i = 1;
			ListaEncadenada<Arco> a =  G.arcosAdyacentesL(v);
			while(i < a.darNumeroElementos())
			{
				Arco e = a.darElemento(i);
				relax(e);
				i++;
			}
		}

		assert check(G, s);
	}
	private void relax(Arco e) {
		int v = e.from(), w = e.to();
		if (distTo[w] > distTo[v] + e.getPeso()) {
			distTo[w] = distTo[v] + e.getPeso();
			edgeTo[w] = e;
			l.agregarElementoFinal(e);
			if (pq.contiene(w)){
				pq.disminuirLlave(w, distTo[w]);
			}
			else{
				pq.insertar(w, distTo[w]);
			}
		}
	}

	public double distTo(int v) {
		validateVertex(v);
		return distTo[v];
	}

	public boolean hasPathTo(int v) {
		validateVertex(v);
		return distTo[v] < Double.POSITIVE_INFINITY;
	}

	public Iterable<Arco> pathTo(int v) {
		validateVertex(v);
		if (!hasPathTo(v)) {
			return null;
		}
		Stack<Arco> path = new Stack<>();
		for (Arco e = edgeTo[v]; e != null; e = edgeTo[e.from()]) 
		{
			path.push(e);
		}
		return path;
	}

	private boolean check(WeightedDirectedGraph G, int s) {

		if (distTo[s] != 0.0 || edgeTo[s] != null) {
			System.err.println("distTo[s] and edgeTo[s] inconsistent");
			return false;
		}
		for (int v = 0; v < G.numVertices(); v++) {
			if (v == s) continue;
			if (edgeTo[v] == null && distTo[v] != Double.POSITIVE_INFINITY) {
				System.err.println("distTo[] and edgeTo[] inconsistent");
				return false;
			}
		}

		for (int v = 0; v < G.numVertices(); v++) {
			while(G.arcosAdyacentes(v).hasNext())
			{
				Arco e = (Arco)G.arcosAdyacentes(v).next();
				int w = e.to();
				if (distTo[v] + e.getPeso() < distTo[w]) {
					System.err.println("edge " + e + " not relaxed");
					return false;
				}
			}
		}

		for (int w = 0; w < G.numVertices(); w++) {
			if (edgeTo[w] == null) continue;
			Arco e = edgeTo[w];
			
			int v = e.from();
			if (w != e.to()) return false;
			if (distTo[v] + e.getPeso() != distTo[w]) {
				System.err.println("edge " + e + " on shortest path not tight");
				return false;
			}
		}
		return true;
	}

	private void validateVertex(int v) {
		int V = distTo.length;
		if (v < 0 || v >= V)
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
	}

}
