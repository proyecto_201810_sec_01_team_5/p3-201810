package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements Iterable<T> {
	
	private ListaEncadenada<T> list;
	
	public Stack(){
		list = new ListaEncadenada<>();
	}
	
	public void push (T item){
		list.agregarElmentoInicio(item);
	}
	
	public T pop(){
		return list.eliminarElemento();
	}
	
	public boolean isEmpty(){
		return (list.darNumeroElementos() == 0);
	}
	
	public int size(){
		return list.darNumeroElementos();
	}

	@Override
	public Iterator<T> iterator() {
		return list.iterator();
	}

	

}
