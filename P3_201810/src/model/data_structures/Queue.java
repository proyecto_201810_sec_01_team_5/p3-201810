package model.data_structures;

import java.util.Iterator;

public class Queue<T>implements Iterable<T> {
	
	private ListaEncadenada<T> list;
	
	public Queue(){
		list = new ListaEncadenada<>();
	}
	
	public void enqueue(T item){
		list.agregarElementoFinal(item);
	}
	
	public T dequeue(){
		return list.eliminarElemento();
	}
	
	public boolean isEmpty(){
		return (list.darNumeroElementos() == 0);
	}
	
	public int size(){
		return list.darNumeroElementos();
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return list.iterator();
	}
	

}
