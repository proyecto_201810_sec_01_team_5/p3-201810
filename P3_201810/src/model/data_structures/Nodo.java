package model.data_structures;

public class Nodo<T> {

	private T dato;
	private Nodo<T> siguiente;

	public Nodo( T pDato, Nodo<T> pSiguiente){
		siguiente = pSiguiente;
		dato = pDato;
	}
	
	public T getDato(){
        return dato;
    }
	
	public Nodo<T> getSiguiente(){
		return siguiente;
	}
	
	public void setDato(T pDato){
        dato = pDato;
    }

    public void setSiguiente(Nodo<T> pSiguiente) {
        siguiente = pSiguiente;
    }

}
