package model.data_structures;

import javax.swing.text.html.HTMLDocument.Iterator;

public class ColaPrioridad<T extends Comparable<T>> {

	private T[] cola;
	private int max;
	private int size;

	public void crearCP(int max) {

		cola= (T[]) new Comparable[max];
		this.max = max;
		size = 0;
	}

	public int darNumeroElementos(){
		return size;
	}

	public void agregar(T elemento) throws Exception{

		if(size < max)
		{
			size++;
			
			int p = size-1;
			
			cola[p]=elemento;
			while(p>0&& !less(cola[p-1], cola[p])){
				exchange(p , p-1);
				p--;
			}
		}
		else
		{
			throw new Exception("Se ha llenado la cola");
		}
	}

	private void exchange(int p, int i) {
		T t = cola[p];
		cola[p]=cola[i];
		cola[i]=t;
		
	}

	private boolean less(T t, T t2) {
		
		return t.compareTo(t2)<0;
	}

	public T max(){

		if(size == 0)
		{
			return null;
		}
		else
		{
			return cola[0];
		}
	}

	public boolean isEmpty(){

		return size == 0;
	}

	public int tamanoMax(){

		return max;
	}
	
	public ListaEncadenada<T> darCola(){
		
		ListaEncadenada<T> rta = new ListaEncadenada<>();
		
		for (int i = 0; i < size ; i++) {
			rta.agregarElmentoInicio(cola[i]);
		}
		return rta;
		
	}

}
