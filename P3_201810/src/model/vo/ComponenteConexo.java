package model.vo;

import model.data_structures.IList;

public class ComponenteConexo implements Comparable<ComponenteConexo> {
	private String color;
	private int numVertices;
	private IList<VerticeConServicios> vertices;
	
	
	public ComponenteConexo(String color) {
		super();
		this.color = color;
//		Inicializar  Lista
	}
	
	public ComponenteConexo(String color, IList<VerticeConServicios> vertices) {
		super();
		this.color = color;
		this.numVertices = vertices.size();
		this.vertices = vertices;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getNumVertices() {
		return numVertices;
	}
	public void setNumVertices(int numVertices) {
		this.numVertices = numVertices;
	}
	
	/**
     * Agrega vertice al listado
     * @param vertice
     */
    public void agregarServicioQueLlega(VerticeConServicios vertice){
        vertices.add(vertice);
        numVertices++;
    }
	
	@Override
	public int compareTo(ComponenteConexo o) {
		return Integer.compare(this.getNumVertices(), o.getNumVertices());
	}
	
	
	
	

}
