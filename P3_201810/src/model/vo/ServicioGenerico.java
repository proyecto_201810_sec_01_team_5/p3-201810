package model.vo;

public class ServicioGenerico 
{
	private String company;
	private String dropoff_census_tract;
	private String dropoff_centroid_latitude;
	//private String dropoff_centroid_location;
	private String dropoff_centroid_longitude;
	private String dropoff_community_area;
	private String extras;
	private String fare;
	private String payment_type;
	private String pickup_census_tract;
	private String pickup_centroid_latitude;
	//private String pickup_centroid_location;
	private String pickup_centroid_longitude;
	private String pickup_community_area;
	private String taxi_id;
	private String tips;
	private String tolls;
	private String trip_end_timestamp;
	private String trip_id;
	private String trip_miles;
	private String trip_seconds;
	private String trip_start_timestamp;
	private String trip_total;
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the dropoff_census_tract
	 */
	public String getDropoff_census_tract() {
		return dropoff_census_tract;
	}
	/**
	 * @param dropoff_census_tract the dropoff_census_tract to set
	 */
	public void setDropoff_census_tract(String dropoff_census_tract) {
		this.dropoff_census_tract = dropoff_census_tract;
	}
	/**
	 * @return the dropoff_centroid_latitude
	 */
	public String getDropoff_centroid_latitude() {
		return dropoff_centroid_latitude;
	}
	/**
	 * @param dropoff_centroid_latitude the dropoff_centroid_latitude to set
	 */
	public void setDropoff_centroid_latitude(String dropoff_centroid_latitude) {
		this.dropoff_centroid_latitude = dropoff_centroid_latitude;
	}
	/**
	 * @return the dropoff_centroid_longitude
	 */
	public String getDropoff_centroid_longitude() {
		return dropoff_centroid_longitude;
	}
	/**
	 * @param dropoff_centroid_longitude the dropoff_centroid_longitude to set
	 */
	public void setDropoff_centroid_longitude(String dropoff_centroid_longitude) {
		this.dropoff_centroid_longitude = dropoff_centroid_longitude;
	}
	/**
	 * @return the dropoff_community_area
	 */
	public String getDropoff_community_area() {
		return dropoff_community_area;
	}
	/**
	 * @param dropoff_community_area the dropoff_community_area to set
	 */
	public void setDropoff_community_area(String dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}
	/**
	 * @return the extras
	 */
	public String getExtras() {
		return extras;
	}
	/**
	 * @param extras the extras to set
	 */
	public void setExtras(String extras) {
		this.extras = extras;
	}
	/**
	 * @return the fare
	 */
	public String getFare() {
		return fare;
	}
	/**
	 * @param fare the fare to set
	 */
	public void setFare(String fare) {
		this.fare = fare;
	}
	/**
	 * @return the payment_type
	 */
	public String getPayment_type() {
		return payment_type;
	}
	/**
	 * @param payment_type the payment_type to set
	 */
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	/**
	 * @return the pickup_centroid_latitude
	 */
	public String getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}
	/**
	 * @param pickup_centroid_latitude the pickup_centroid_latitude to set
	 */
	public void setPickup_centroid_latitude(String pickup_centroid_latitude) {
		this.pickup_centroid_latitude = pickup_centroid_latitude;
	}
	/**
	 * @return the pickup_census_tract
	 */
	public String getPickup_census_tract() {
		return pickup_census_tract;
	}
	/**
	 * @param pickup_census_tract the pickup_census_tract to set
	 */
	public void setPickup_census_tract(String pickup_census_tract) {
		this.pickup_census_tract = pickup_census_tract;
	}
	/**
	 * @return the pickup_centroid_longitude
	 */
	public String getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}
	/**
	 * @param pickup_centroid_longitude the pickup_centroid_longitude to set
	 */
	public void setPickup_centroid_longitude(String pickup_centroid_longitude) {
		this.pickup_centroid_longitude = pickup_centroid_longitude;
	}
	/**
	 * @return the pickup_community_area
	 */
	public String getPickup_community_area() {
		return pickup_community_area;
	}
	/**
	 * @param pickup_community_area the pickup_community_area to set
	 */
	public void setPickup_community_area(String pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}
	/**
	 * @return the taxi_id
	 */
	public String getTaxi_id() {
		return taxi_id;
	}
	/**
	 * @param taxi_id the taxi_id to set
	 */
	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}
	/**
	 * @return the tips
	 */
	public String getTips() {
		return tips;
	}
	/**
	 * @param tips the tips to set
	 */
	public void setTips(String tips) {
		this.tips = tips;
	}
	/**
	 * @return the tolls
	 */
	public String getTolls() {
		return tolls;
	}
	/**
	 * @param tolls the tolls to set
	 */
	public void setTolls(String tolls) {
		this.tolls = tolls;
	}
	/**
	 * @return the trip_end_timestamp
	 */
	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}
	/**
	 * @param trip_end_timestamp the trip_end_timestamp to set
	 */
	public void setTrip_end_timestamp(String trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}
	/**
	 * @return the trip_id
	 */
	public String getTrip_id() {
		return trip_id;
	}
	/**
	 * @param trip_id the trip_id to set
	 */
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	/**
	 * @return the trip_miles
	 */
	public String getTrip_miles() {
		return trip_miles;
	}
	/**
	 * @param trip_miles the trip_miles to set
	 */
	public void setTrip_miles(String trip_miles) {
		this.trip_miles = trip_miles;
	}
	/**
	 * @return the trip_seconds
	 */
	public String getTrip_seconds() {
		return trip_seconds;
	}
	/**
	 * @param trip_seconds the trip_seconds to set
	 */
	public void setTrip_seconds(String trip_seconds) {
		this.trip_seconds = trip_seconds;
	}
	/**
	 * @return the trip_start_timestamp
	 */
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}
	/**
	 * @param trip_start_timestamp the trip_start_timestamp to set
	 */
	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}
	/**
	 * @return the trip_total
	 */
	public String getTrip_total() {
		return trip_total;
	}
	/**
	 * @param trip_total the trip_total to set
	 */
	public void setTrip_total(String trip_total) {
		this.trip_total = trip_total;
	}
}
