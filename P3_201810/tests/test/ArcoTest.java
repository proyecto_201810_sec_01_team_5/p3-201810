package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Arco;
import model.data_structures.ListaEncadenada;
import model.data_structures.Vertice;
import model.vo.TaxiConServicios;

public class ArcoTest 
{
	private Arco arco;
	/**
     * Crea una instancia de la clase Arco. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
		Vertice salida=null;
		Vertice llegada=null;
		double ppeso=2;
		char porden='a';

        arco = new Arco(salida, llegada, ppeso, porden);
    }
    
	
	@Test
    public void test1()
    {
    	assertTrue("El peso del arco no es correcto", arco.getPeso()==2);
    }
	
	@Test
    public void test2()
    {
		arco.setPeso(44);
    	assertTrue("El peso del arco no es correcto", arco.getPeso()==44);
    }
    
}
